//
//  ContentView.swift
//  ForumApp
//
//  Created by Marco Longobardi on 08/09/21.
//

import SwiftUI

struct HomeView: View {

    @AppStorage("token9") var token: String = ""
    @State var showSignUp = false
    
    
    var body: some View {
        
        if  token != ""  {
            ZStack {
                Color.black.ignoresSafeArea()
                TabView {
                    NewsView().tabItem { VStack {Image(systemName: "newspaper")
                        Text("News")
                    } }.tag(1)
                    EventsView().tabItem { VStack {Image(systemName: "star")
                        Text("Eventi")
                    } }.tag(3)
                    AuleStudioView().tabItem { VStack {Image(systemName: "book")
                        Text("Aula")
                    } }.tag(4)
                    InfoForumView().tabItem { VStack {Image(systemName: "info.circle")
                        Text("Chi siamo")
                    } }.tag(5)
                    UserDetailsView().tabItem { VStack {Image(systemName: "person")
                        Text("Profilo")
                    } }.tag(6)
                }
            }        }
        else {
        VStack {
            
            ZStack{
                if showSignUp{
                    RegisterView()
                        .transition(.move(edge: .trailing))
                }
                else {
                    LoginView()
                        .transition(.move(edge: .trailing))

                }
            }
            .frame(maxHeight: .infinity, alignment: .leading)

            .padding()
            .overlay(
                HStack{
                    Text(showSignUp ? "Già Membro?" : "Nuovo Membro?")
                        .fontWeight(.bold)
                        .foregroundColor(.gray)
                    
                    Button(action: {withAnimation{showSignUp.toggle()}}, label: {
                        Text(showSignUp ?  "Accedi" : "Registrati")
                            .fontWeight(.bold)
                            .foregroundColor(.blue)
                    })
                },alignment: .bottom
            )
        }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
