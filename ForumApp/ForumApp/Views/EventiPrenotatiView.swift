//
//  EventiPrenotatiView.swift
//  ForumApp
//
//  Created by Marco Longobardi on 09/09/21.
//

import SwiftUI

struct EventiPrenotatiView: View {
    
    var eventi: [Eventi]
    
    var prenotazioniAula : [PrenotazioniAula]
    
    var body: some View {
        
        VStack {
            VStack {
                Text("PRENOTAZIONI EVENTI")
                    .padding(.top)
                    .frame(width: 350, height: 50, alignment: .topLeading)
                    .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                List(eventi){ evento in
                    
                        
                        VStack(alignment: .leading, spacing: 5) {
                            Text(evento.titolo)
                                .fontWeight(.semibold)
                                .lineLimit(2)
                                .minimumScaleFactor(0.5)
                            
                            Text(evento.data)
                                .font(.subheadline)
                                .foregroundColor(.secondary)
                        
                    }
                    
                    
                    
                }
            }
            VStack {
                Text("PRENOTAZIONI AULA")
                    .frame(width: 350, height: 50, alignment: .topLeading)
                    .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                

                List(prenotazioniAula){ prenotazione in
                    

                        VStack(alignment: .leading, spacing: 5) {
                            Text("Aula \(prenotazione.aula)")
                                .fontWeight(.semibold)
                                .lineLimit(2)
                                .minimumScaleFactor(0.5)
                            
                            Text(prenotazione.data)
                                .font(.subheadline)
                                .foregroundColor(.secondary)
                        

                        
                    }
                    
                    
                    
                }
            }
        }
    }
}

struct EventiPrenotatiView_Previews: PreviewProvider {
    static var previews: some View {
        EventiPrenotatiView(eventi: [Eventi](), prenotazioniAula: [PrenotazioniAula]())
    }
}
