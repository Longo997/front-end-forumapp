//
//  LoginView.swift
//  ForumApp
//
//  Created by Marco Longobardi on 08/09/21.
//

import SwiftUI

struct LoginView: View {
    
    @StateObject var loginVM = LoginViewModel()

    
    
    var body: some View {
            
            VStack{
                Text("Sign In")
                    .font(.title)
                    .fontWeight(.bold)
                    .foregroundColor(Color(.black))
                    .kerning(1.9)
                    .frame(maxWidth: .infinity, alignment: .leading)
                
                VStack(alignment: .leading, spacing: 8, content: {
                    Text("User Name")
                        .fontWeight(.bold)
                        .foregroundColor(.gray)
                    
                    TextField("prova@gmail.com", text: $loginVM.mail)
                        .font(.system(size: 20, weight: .semibold))
                        .foregroundColor(Color(.black))
                        .padding(.top, 5)
                })
                .padding(.top,25)
                
                VStack(alignment: .leading, spacing: 8, content: {
                    Text("Password")
                        .fontWeight(.bold)
                        .foregroundColor(.gray)
                    
                    SecureField("123456", text: $loginVM.password)
                        .font(.system(size: 20, weight: .semibold))
                        .foregroundColor(Color(.black))
                        .padding(.top, 5)
                    
                    
                })
                .padding(.top,25)
                
                Button(action: {loginVM.login()}, label: {
                                    Image(systemName: "arrow.right")
                                        .font(.system(size:24, weight: .bold))
                                        .foregroundColor(.white)
                                        .padding()
                                        .background(Color(.black))
                                        .clipShape(Circle())
                                        .shadow(color: .blue, radius: 5, x: 0, y: 0)
                                })
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.top, 10)
            }
            .frame(maxHeight: .infinity, alignment: .leading)

            .padding()
        
        
        
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
