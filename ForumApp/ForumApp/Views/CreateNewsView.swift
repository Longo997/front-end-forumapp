//
//  CreateNewsView.swift
//  ForumApp
//
//  Created by Marco Longobardi on 08/09/21.
//

import SwiftUI

struct CreateNewsView: View {
    let defaults = UserDefaults.standard

    
    @Environment(\.presentationMode) var presentationMode
    
    //show image picker
    @State var showImagePicker: Bool = false
    
    //show selected image
    @State var selectedImage: Image? = Image("")
    
    @State var titolo: String = ""
    @State var testo: String = ""
    let formatter = DateFormatter()
    
        
    var body: some View {
        
        ZStack{
            
            ScrollView{
                VStack{
                    
                    Text("CREA NUOVA NEWS")
                     .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                     .fontWeight(.bold)
                     .foregroundColor(Color(.black))
                     .multilineTextAlignment(.leading)
                     .padding(.top)
                     Spacer()
                     Spacer()

                                         
                     VStack {
                         Text("Inserisci titolo news:")
                             .font(.title2)
                             .fontWeight(.light)
                             .frame(width: 363, height: 30, alignment: .topLeading)
                         
                         TextField(" Titolo qui:", text: self.$titolo)
                             .frame(width: 363, height: 50,  alignment: .topLeading)
                             .background(Color(#colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)))
                             .cornerRadius(7)
                     }
                    
                                        
                  
                    VStack {
                        Text("Inserisci testo: ")
                            .font(/*@START_MENU_TOKEN@*/.title2/*@END_MENU_TOKEN@*/)
                            .fontWeight(.light)
                            .frame(width: 363, height: 30, alignment: .topLeading)
                        TextField(" Corpo qui:", text: self.$testo)
                            .frame(width: 363, height: 150,  alignment: .topLeading)
                            .background(Color(#colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)))
                            .cornerRadius(7)
                    }
                    .padding(.vertical)
                    
                    VStack{
                        HStack{
                            Text("Carica immagine:")
                                .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                                .frame(width: 210, height: 50, alignment: .topLeading)
                            Button(action: {
                                self.showImagePicker.toggle()
                            }) {
                                Text("Sfoglia")
                                    .fontWeight(.bold)
                                    .font(.body)
                                    .background(Color(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)))
                                    .foregroundColor(.black)
                                    .padding()
                                    .background(Color(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)))
                                    .frame(width: 145, height: 50)
                            }
                            .background(Color(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)))
                            .cornerRadius(10)

                        }
                        //show image
                        
                        
                        
                    }.sheet(isPresented: $showImagePicker, content: {
                        ImagePicker(image: self.$selectedImage)
                    })
                    
                    
                    
                    
                                                
                    HStack{
                        Button(action: {
                            print("dismisses form")
                                            self.presentationMode.wrappedValue.dismiss()
                        }) {
                            Text("Annulla")
                                .fontWeight(.bold)
                                .font(.body)
                                .background(Color(#colorLiteral(red: 1, green: 0.3019607843, blue: 0.3019607843, alpha: 1)))
                                .foregroundColor(.white)
                                .padding()
                                .background(Color(#colorLiteral(red: 1, green: 0.3019607843, blue: 0.3019607843, alpha: 1)))
                                .frame(width: 168, height: 60)
                        }
                        .background(Color(#colorLiteral(red: 1, green: 0.3019607843, blue: 0.3019607843, alpha: 1)))
                        .cornerRadius(10)
                        
                        Button(action: {
                            let nomeImg = randomString(of: 20)
                            formatter.dateFormat = "dd-MM-yyyy"
                            let result = formatter.string(from: Date())
                            NewsViewModel().createNews(news: News( data: result, titolo: titolo,testo: testo,  immagine: nomeImg), token: defaults.string(forKey: "token9")!, immagine: (selectedImage?.asUIImage())!, completion: {response in })
                        }) {
                            Text("Conferma")
                                .fontWeight(.bold)
                                .font(.body)
                                .background(Color(#colorLiteral(red: 0.9868217111, green: 0.6015813947, blue: 0.3579081297, alpha: 1)))
                                .foregroundColor(.white)
                                .padding()
                                .background(Color(#colorLiteral(red: 0.9868217111, green: 0.6015813947, blue: 0.3579081297, alpha: 1)))
                                .frame(width: 168, height: 60)
                        }
                        .background(Color(#colorLiteral(red: 0.9868217111, green: 0.6015813947, blue: 0.3579081297, alpha: 1)))
                        .cornerRadius(10)
                    }
                    .padding(.top, 80.0)
                }
            }
        }
        .frame(width: 400)
        
    }
}

struct CreateNewsView_Previews: PreviewProvider {
    static var previews: some View {
        CreateNewsView()
    }
}
