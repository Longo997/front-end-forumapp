//
//  InfoForumView.swift
//  ForumApp
//
//  Created by Marco Longobardi on 08/09/21.
//

import SwiftUI

struct InfoForumView: View {
    var body: some View {
        ZStack{
            
            ScrollView{
                VStack{
                    //Image("\(notizia.immagine)")
                    Image("logo")
                        .resizable()
                        .scaledToFit()
                        .frame(height:200)
                    
                    Text("CHI SIAMO")
                        .font(/*@START_MENU_TOKEN@*/.largeTitle/*@END_MENU_TOKEN@*/)
                        .foregroundColor(Color(.black))
                    Spacer()
                    HStack {
                        Text("Siamo il Forum dei Giovani di Pompei, attivo da Luglio 2019. Promotore sul territorio di una pluralità di iniziative, eventi e manifestazioni aventi come destinatari i giovani. Da sempre attento ai loro bisogni, capace di ascoltare la loro voce, di raccogliere le loro idee, di credere in un futuro migliore e lottare per i loro sogni, perché ciò che è loro fa parte di noi, del nostro essere giovani attivi.Il Forum è un organismo di partecipazione a carattere elettivo, che si propone di avvicinare i giovani alle Istituzioni e le Istituzioni al mondo dei giovani. ")
                            .fontWeight(.light)
                            .multilineTextAlignment(.leading)
                            .lineLimit(nil)
                            .frame(width: 350, height: 500, alignment: .leading)
                            .font(.title2)
                    }.background(Color(#colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)))
                    
        
                }
                }
            }//.frame(width: 400)
       
        }
}

struct InfoForumView_Previews: PreviewProvider {
    static var previews: some View {
        InfoForumView()
    }
}
