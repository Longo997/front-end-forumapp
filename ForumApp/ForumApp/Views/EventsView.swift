//
//  EventsView.swift
//  ForumApp
//
//  Created by Marco Longobardi on 08/09/21.
//

import SwiftUI

struct EventsView: View {
    
    @State var backgroundOffset: CGFloat = 0
    @State private var show_modal: Bool = false
    @StateObject var eventiVM = EventsViewModel()
    let defaults = UserDefaults.standard
    @StateObject var storServ = StorageService()
    
    
    var body: some View {
        NavigationView {
            VStack {
                
                
                List(eventiVM.eventi){ evento in
                    NavigationLink(
                        destination: EventDetailsView(evento: evento , immagine: (storServ.imageCache[evento.immagine] ?? UIImage(named: "logo"))!)){
                        
                        
                        ZStack {
                            Color(.white)
                            VStack (alignment: .leading){
                                HStack {
                                    Image(uiImage: (storServ.imageCache[evento.immagine] ?? UIImage(named: "logo"))!)
                                        .resizable()
                                        .scaledToFit()
                                        .frame(height: 90)
                                    
                                    Text(evento.titolo)
                                        .font(.headline)
                                        //.fontWeight(.bold)
                                        .fontWeight(.light)
                                        .foregroundColor(.black)
                                        
                                        .frame(height: 90)
                                        .padding(.bottom)
                                        .padding(.leading)
                                        .padding(.trailing)
                                        
                                        .padding(.top)
                                }
                                
                                
                                    
                                    
                                    Text(evento.data)
                                        .font(.body)
                                        .fontWeight(.ultraLight)
                                        .foregroundColor(.black)
                                        
                                        .padding(.leading)
                                
                            }.onAppear(){
                                storServ.getImage(image: evento.immagine)
                        }
                        }.cornerRadius(4)
                        .padding(.bottom)
                    }
                    
                    
                    
                }
                
                .onAppear() {
                    eventiVM.getEvents()
                }
                
                
            }.navigationTitle("EVENTI")
            .toolbar {
                ToolbarItem(placement: .primaryAction) {
                    if (defaults.string(forKey: "ruolo")! == "organizzatore"){
                        Button(action: {
                                self.show_modal = true                        }) {
                            Image(systemName: "plus.circle")
                                .foregroundColor(Color.black)
                            
                        }.sheet(isPresented: self.$show_modal) {
                            CreateEventView()
                        }
                    }
                }
            }
        }
    }
    
}

struct EventsView_Previews: PreviewProvider {
    static var previews: some View {
        EventsView()
    }
}
