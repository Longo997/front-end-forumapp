//
//  EventDetailsView.swift
//  ForumApp
//
//  Created by Marco Longobardi on 08/09/21.
//

import SwiftUI

struct EventDetailsView: View {
    var evento : Eventi
    let defaults = UserDefaults.standard
    var immagine : UIImage

    
    var body: some View {
        ScrollView {
            VStack(spacing: 20){
                
                Spacer()
                
                Image(uiImage: immagine)
                    .resizable()
                    .scaledToFit()
                    .frame(height: 300)
    //                .cornerRadius(12)
                
    //            Text(evento.titolo)
    //                .font(.title2)
    //                .fontWeight(.semibold)
    //                .lineLimit(2)
    //                .multilineTextAlignment(.center)
    //                .padding(.horizontal)
                
                HStack(spacing: 40){
                    
                    Text(evento.data)
                        .font(.subheadline)
                        .foregroundColor(.secondary)
                }
                
                Text(evento.descrizione)
                    .font(.body)
                    .fontWeight(.light)
                    .frame(width: 350, alignment: .topLeading)
                    .padding()
                    .lineLimit(nil)
                
                Spacer()
                
                Button(action: {EventsViewModel().prenotaEvento(idEvento: evento.id!.uuidString, idUtente: defaults.string(forKey: "userID")!, token: defaults.string(forKey: "token9")!, completion: {response in print(response)})}, label: {
                    Text("Prenota Evento")
                        .fontWeight(.bold)
                        .bold()
                        .font(.body)
                        .frame(width: 168, height: 60)
                        .background(Color(#colorLiteral(red: 0.9568627451, green: 0.5960784314, blue: 0.368627451, alpha: 1)))
                        .foregroundColor(.white)
                        .cornerRadius(10)

    //                    .cornerRadius(10)
                })
                    
                
                Spacer()
                
            }
        }
    }
}

struct EventDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        EventDetailsView(evento: Eventi(data:"15/06/2022", titolo: "Prova", numPosti: 5, immagine: ""), immagine: UIImage())
    }
}
