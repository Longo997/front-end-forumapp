//
//  UserDetailsView.swift
//  ForumApp
//
//  Created by Marco Longobardi on 08/09/21.
//

import SwiftUI

struct UserDetailsView: View {
    
    @StateObject var userDetailsVM = UserDetailsViewModel()
    let defaults = UserDefaults.standard
    @State private var show_modal: Bool = false


    
    var body: some View {
        ZStack{
            
            ScrollView{
                VStack{
                    //Image("\(notizia.immagine)")
                    Image("logo")
                        .resizable()
                        .scaledToFit()
                        .frame(height:200)
                    
                    Text("INFO UTENTE")
                        .font(/*@START_MENU_TOKEN@*/.title2/*@END_MENU_TOKEN@*/)
                        .foregroundColor(Color(.black))
                        .multilineTextAlignment(.leading)
                        .padding(.top)
                    //Spacer()
                    VStack {
                        Text("Nome: \(userDetailsVM.utente.nome)")
                            .fontWeight(.light)
                            .multilineTextAlignment(.leading)
                            .frame(width: 350, height: 50, alignment: .leading)
                            .font(.title2)
                        Text("Cognome: \(userDetailsVM.utente.cognome)")
                                                .fontWeight(.light)
                                                .multilineTextAlignment(.leading)
                                                .frame(width: 350, height: 50, alignment: .leading)
                                                .font(.title2)
                    }.background(Color(#colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)))
                    Spacer()
                    
               
                    Text("Prenotazioni Effettuate:")
                        .font(.callout)
                        .fontWeight(.light)
                        .italic()
                        .padding(.top)
                HStack{
                    Button(action: {
                        self.show_modal = true
                        
                    }) {
                        Text("Mostra")
                            .fontWeight(.bold)
                            .font(.body)
                            .background(Color(#colorLiteral(red: 0.9868217111, green: 0.6015813947, blue: 0.3579081297, alpha: 1)))
                            .foregroundColor(.white)
                            .padding()
                            .background(Color(#colorLiteral(red: 0.9868217111, green: 0.6015813947, blue: 0.3579081297, alpha: 1)))
                            .border(Color(#colorLiteral(red: 0.9868217111, green: 0.6015813947, blue: 0.3579081297, alpha: 1)))
                            .frame(width: 168, height: 60)
                    }.sheet(isPresented: self.$show_modal) {EventiPrenotatiView(eventi: userDetailsVM.eventiPrenotati, prenotazioniAula: userDetailsVM.prenotazioniAula)
                      }
                    
                    .background(Color(#colorLiteral(red: 0.9868217111, green: 0.6015813947, blue: 0.3579081297, alpha: 1)))
                    .cornerRadius(10)
                    
                    
                
                      }
       
                    .background(Color(#colorLiteral(red: 0.9868217111, green: 0.6015813947, blue: 0.3579081297, alpha: 1)))
                    .cornerRadius(10)
        
                }.onAppear(){
                    userDetailsVM.getInfo(token: defaults.string(forKey: "token9")!)
                    userDetailsVM.getEventiPrenotati(id: defaults.string(forKey: "userID")!, token: defaults.string(forKey: "token9")!)
                    userDetailsVM.getPrenotazioniAula(id: defaults.string(forKey: "userID")!)
                                            
                }
                }
            }.frame(width: 400)
       
        }
}

struct UserDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        UserDetailsView()
    }
}
