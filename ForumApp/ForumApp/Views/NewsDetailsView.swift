//
//  NewsDetailsView.swift
//  ForumApp
//
//  Created by Marco Longobardi on 08/09/21.
//

import SwiftUI

struct NewsDetailsView: View {
    var notizia: News
    var immagine: UIImage
    
    var body: some View {
        ZStack{
            
            ScrollView{
                VStack{
                    //Image("\(notizia.immagine)")
                    Image(uiImage: immagine)
                        .resizable()
                        .scaledToFit()
                    Text("\(notizia.titolo)")
                        .frame(width: 350, alignment: .topLeading)
                        .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                    Spacer()
                    Text("\(notizia.dataPubblicazione)")
                        .frame(width: 350, alignment: .topLeading)
                        .font(.title2)
                    Spacer()
                    Text("\(notizia.testo)")
                        .fontWeight(.light)
                        .frame(width: 350, alignment: .topLeading)
                        .font(.title2)
                        .lineLimit(nil)
                }
            }
        }.frame(width: 400)
        
    }
}

struct NewsDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        NewsDetailsView(notizia: News(id: UUID.init(), data: "20 ottobre", titolo: "viva la vita", testo: "hgfuinvwqergergqergeqbebqetbeqrbqerbeqfbefqbfbefbqbq4ebtbwrtbwtn4trnbwrtnrwtnrtnwrnrwnrnhgfuinvwqergergqergeqbebqetbeqrbqerbeqfbefqbfbefbqbq4ebtbwrtbwtn4trnbwrtnrwtnrtnwrnrwnrn", immagine: "boh"), immagine: UIImage())
    }
}
