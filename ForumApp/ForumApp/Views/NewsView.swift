//
//  NewsView.swift
//  ForumApp
//
//  Created by Marco Longobardi on 08/09/21.
//

import SwiftUI

struct NewsView: View {
    
    @State private var show_modal: Bool = false
    @StateObject var newsVM = NewsViewModel()
    @StateObject var storageService = StorageService()
    let defaults = UserDefaults.standard



    
    
    
    var body: some View {
        
        
        NavigationView {
            
            VStack {
                
                List(newsVM.news) { notizia in
                    
                    NavigationLink(
                        destination: NewsDetailsView(notizia: notizia, immagine: (storageService.imageCache[notizia.immagine] ?? UIImage(named: "logo"))! ) ){
                        
                        ZStack {
                            Color(.white)
                            VStack(alignment: .leading) {
                                
                                
                                HStack {
                                    Image(uiImage: (storageService.imageCache[notizia.immagine] ?? UIImage(named: "logo"))!)
                                        .resizable()
                                        .scaledToFit()
                                        .frame(height: 90)
                                    
                                    Text("\(notizia.titolo)")
                                                                        .font(.headline)
                                                                        //.fontWeight(.bold)
                                        .fontWeight(.light)
                                        .foregroundColor(.black)
                                        
                                                .frame(height: 90)
                                                                        .padding(.bottom)
                                                                        .padding(.leading)
                                                                        .padding(.trailing)
                                        
                                            .padding(.top)
                                }
                                
                                
                                
                                Text("\(notizia.dataPubblicazione)")
                                    .font(.body)
                                    .fontWeight(.ultraLight)
                                    .foregroundColor(.black)
                                    
                                    .padding(.leading)
                                
                            }
                            .onAppear(){
                                storageService.getImage(image: notizia.immagine)
                        }
                        }.cornerRadius(4)
                        .padding(.bottom)
                        
                    }
                    
                }
                .onAppear() {
                    newsVM.getNews()
                    
                }.navigationTitle("ULTIME NEWS")
                .toolbar {
                    ToolbarItem(placement: .primaryAction) {
                        if (defaults.string(forKey: "ruolo")! == "organizzatore"){
                        Button(action: {
                                self.show_modal = true                        }) {
                            Image(systemName: "plus.circle")
                                .foregroundColor(Color.black)
                                
                        }.sheet(isPresented: self.$show_modal) {
                            CreateNewsView()
                          }
                    }
                        
                    }
                }
                
            }
            
        }
    }
}

struct NewsView_Previews: PreviewProvider {
    static var previews: some View {
        NewsView()
    }
}
