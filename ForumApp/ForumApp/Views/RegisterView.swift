//
//  RegisterView.swift
//  ForumApp
//
//  Created by Marco Longobardi on 08/09/21.
//

import SwiftUI

struct RegisterView: View {
    
    @StateObject var registerVM = RegisterViewModel()

    
    var body: some View {
            
            VStack{
                Text("Sign Up")
                    .font(.title)
                    .fontWeight(.bold)
                    .foregroundColor(Color(.black))
                    .kerning(1.9)
                    .frame(maxWidth: .infinity, alignment: .leading)
                
                VStack(alignment: .leading, spacing: 8, content: {
                    Text("Nome")
                        .fontWeight(.bold)
                        .foregroundColor(.gray)
                    
                    TextField("Federico", text: $registerVM.nome)
                        .font(.system(size: 20, weight: .semibold))
                        .foregroundColor(Color(.black))
                        .padding(.top, 5)
                })
                .padding(.top,25)
                
                VStack(alignment: .leading, spacing: 8, content: {
                    Text("Cognome")
                        .fontWeight(.bold)
                        .foregroundColor(.gray)
                    
                    TextField("Santilli", text: $registerVM.cognome)
                        .font(.system(size: 20, weight: .semibold))
                        .foregroundColor(Color(.black))
                        .padding(.top, 5)
                })
                .padding(.top,25)
                
                VStack(alignment: .leading, spacing: 8, content: {
                    Text("User Name")
                        .fontWeight(.bold)
                        .foregroundColor(.gray)
                    
                    TextField("prova@gmail.com", text: $registerVM.mail)
                        .font(.system(size: 20, weight: .semibold))
                        .foregroundColor(Color(.black))
                        .padding(.top, 5)
                })
                .padding(.top,25)
                
                VStack(alignment: .leading, spacing: 8, content: {
                    Text("Password")
                        .fontWeight(.bold)
                        .foregroundColor(.gray)
                    
                    SecureField("123456", text: $registerVM.password)
                        .font(.system(size: 20, weight: .semibold))
                        .foregroundColor(Color(.black))
                        .padding(.top, 5)
                    
                    
                })
                .padding(.top,25)
                
                VStack(alignment: .leading, spacing: 8, content: {
                    Text("Confirm Password")
                        .fontWeight(.bold)
                        .foregroundColor(.gray)
                    
                    SecureField("123456", text: $registerVM.confirmPassword)
                        .font(.system(size: 20, weight: .semibold))
                        .foregroundColor(Color(.black))
                        .padding(.top, 5)
                    
                    
                })
                .padding(.top,25)
                
                Button(action: {registerVM.register()}, label: {
                                    Image(systemName: "arrow.right")
                                        .font(.system(size:24, weight: .bold))
                                        .foregroundColor(.white)
                                        .padding()
                                        .background(Color(.black))
                                        .clipShape(Circle())
                                        .shadow(color: .blue, radius: 5, x: 0, y: 0)
                                })
                .frame(maxWidth: .infinity, alignment: .leading)
                .padding(.top, 10)
            }
            .frame(maxHeight: .infinity, alignment: .leading)

            .padding()
        
        

    }
}
