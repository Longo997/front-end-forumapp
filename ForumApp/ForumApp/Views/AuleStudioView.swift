//
//  AuleStudioView.swift
//  ForumApp
//
//  Created by Marco Longobardi on 08/09/21.
//

import SwiftUI

struct AuleStudioView: View {
    
    @StateObject var auleVM = AuleStudioViewModel()
    let defaults = UserDefaults.standard

    
    var body: some View {
        
        ZStack{
            
            VStack{
                
                Image("aula")
                    .resizable()
                    .frame(width: 400, height: 250, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                
                
                
                Text("SELEZIONA DATA:")
                    .frame(width: 350, height: 50, alignment: .topLeading)
                    .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                    .padding(.bottom, 50)
                
                DatePicker(selection: $auleVM.data,displayedComponents: .date, label: { Text("Data") })
                    .padding(.bottom, 50.0)
                    .scaledToFit()
                                
                HStack{
                    
                    
                    Button(action: {
                        auleVM.prenotazioneAule(user: defaults.string(forKey: "userID")!, token: defaults.string(forKey: "token9")!, completion: {result in})
                    }) {
                        Text("Conferma")
                            .fontWeight(.bold)
                            .font(.body)
                            .frame(width: 168, height: 60)
                            .background(Color(#colorLiteral(red: 0.9568627451, green: 0.5960784314, blue: 0.368627451, alpha: 1)))
                            .foregroundColor(.white)
                            .cornerRadius(10)
                    }
                    
                }
                
                Spacer()
                
            }
            
        }.frame(width: 400)
        
                
    }
}

struct AuleStudioView_Previews: PreviewProvider {
    static var previews: some View {
        AuleStudioView()
    }
}
