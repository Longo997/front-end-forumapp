//
//  CreateEventsView.swift
//  ForumApp
//
//  Created by Marco Longobardi on 08/09/21.
//

import SwiftUI

struct CreateEventView: View {
    let defaults = UserDefaults.standard

    
    @Environment(\.presentationMode) var presentationMode
    
    //show image picker
    @State var showImagePicker: Bool = false
    
    //show selected image
    @State var selectedImage: Image? = Image("")
    @State var titolo: String = ""
    @State var descrizione: String = ""
    @State var numPosti : Int = 2
    @State var data: String = "03/08/2021"
    
        
    var body: some View {
        
        ZStack{
            
            ScrollView{
                VStack{
                    
                   Text("CREA NUOVO EVENTO")
                    .font(/*@START_MENU_TOKEN@*/.title/*@END_MENU_TOKEN@*/)
                    .fontWeight(.bold)
                    .foregroundColor(Color(.black))
                    .multilineTextAlignment(.leading)
                    .padding(.top)
                    Spacer()
                    Spacer()

                                        
                    VStack {
                        Text("Inserisci titolo evento:")
                            .font(.title2)
                            .fontWeight(.light)
                            .frame(width: 363, height: 30, alignment: .topLeading)
                        
                        TextField(" Titolo qui:", text: self.$titolo)
                            .frame(width: 363, height: 50,  alignment: .topLeading)
                            .background(Color(#colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)))
                            .cornerRadius(7)
                    }
                

                    VStack {
                        Text("Inserisci descrizione: ")
                            .font(/*@START_MENU_TOKEN@*/.title2/*@END_MENU_TOKEN@*/)
                            .fontWeight(.light)
                            .frame(width: 363, height: 30, alignment: .topLeading)
                        TextField(" Corpo qui:", text: self.$descrizione)
                            .frame(width: 363, height: 150,  alignment: .topLeading)
                            .background(Color(#colorLiteral(red: 0.9450980392, green: 0.9450980392, blue: 0.9450980392, alpha: 1)))
                            .cornerRadius(7)
                    }
                    .padding(.vertical)

                    
                    
                    VStack{
                        HStack{
                            Text("Carica copertina:")
                                .font(/*@START_MENU_TOKEN@*/.title2/*@END_MENU_TOKEN@*/)
                                .fontWeight(.light)
                                .padding(.top, 1.0)
                                .frame(width: 210, height: 25, alignment: .topLeading)
                            Button(action: {
                                self.showImagePicker.toggle()
                            }) {
                                Text("Sfoglia")
                                    .fontWeight(.bold)
                                    .font(.body)
                                    .background(Color(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)))
                                    .foregroundColor(.black)
                                    .padding()
                                    .background(Color(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)))
                                    .frame(width: 145, height: 50)
                            }
                            .background(Color(#colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)))
                            .cornerRadius(10)
                        }
                        //show image
//                        self.selectedImage?.resizable().scaledToFit()
                        
                        //show button to upload
                       
                        
                    }.sheet(isPresented: $showImagePicker, content: {
                        ImagePicker(image: self.$selectedImage)
                    })
                    
                                                
                    HStack{
                        Button(action: {
                            print("dismisses form")
                                            self.presentationMode.wrappedValue.dismiss()
                        }) {
                            Text("Annulla")
                                .fontWeight(.bold)
                                .font(.body)
                                .background(Color(#colorLiteral(red: 1, green: 0.3019607843, blue: 0.3019607843, alpha: 1)))
                                .foregroundColor(.white)
                                .padding()
                                .background(Color(#colorLiteral(red: 1, green: 0.3019607843, blue: 0.3019607843, alpha: 1)))
                                .frame(width: 168, height: 60)
                        }
                        .background(Color(#colorLiteral(red: 1, green: 0.3019607843, blue: 0.3019607843, alpha: 1)))
                        .cornerRadius(10)
                        
                        Button(action: {
                            let nomeImg = randomString(of: 20)
                            
                            EventsViewModel().createEvent(evento: Eventi( data: data, titolo: titolo,numPosti : numPosti, descrizione: descrizione, immagine: nomeImg), token: defaults.string(forKey: "token9")!, immagine: (selectedImage?.asUIImage())!, completion: {response in print(response)})
                        }) {
                            Text("Conferma")
                                .fontWeight(.bold)
                                .font(.body)
                                .background(Color(#colorLiteral(red: 0.9868217111, green: 0.6015813947, blue: 0.3579081297, alpha: 1)))
                                .foregroundColor(.white)
                                .padding()
                                .background(Color(#colorLiteral(red: 0.9868217111, green: 0.6015813947, blue: 0.3579081297, alpha: 1)))
                                .frame(width: 168, height: 60)
                        }
                        .background(Color(#colorLiteral(red: 0.9868217111, green: 0.6015813947, blue: 0.3579081297, alpha: 1)))
                        .cornerRadius(10)
                    }
                    .padding(.top, 80.0)
                }
            }
        }
        .frame(width: 400)
    }
}

struct CreateEventsView_Previews: PreviewProvider {
    static var previews: some View {
        CreateEventView()
    }
}
