//
//  Utilities.swift
//  ForumApp
//
//  Created by Marco Longobardi on 31/07/21.
//

import Foundation


func randomString(of length: Int) -> String {
    let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    var s = ""
    for _ in 0 ..< length {
        s.append(letters.randomElement()!)
    }
    return s
}


