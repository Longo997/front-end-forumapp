//
//  StorageService.swift
//  ForumApp
//
//  Created by Marco Longobardi on 09/09/21.
//

import Foundation
import SwiftUI
import Amplify

class StorageService : ObservableObject {
    
    @Published var imageCache = [String: UIImage?]()
    
    func getImage(image: String){
        
        
        downloadImage(image: image)  {result in
            switch result {
            case .success(let uiImage):
                return uiImage
            case .failure(_):
                return UIImage(named: "logo")!
            }
        }
    }
    
    
    func uploadImage(immagine: UIImage, nome : String) {
        let fileKey = nome + ".jpg"
        guard let fileContents = immagine.jpegData(compressionQuality: 0.5) else {return }
        print("Sto facendo")
        Amplify.Storage.uploadData(key: fileKey, data: fileContents) { result in
            switch result {
            case .success(let key):
                print("File with \(key) uploaded")
                
            case .failure(let storageError):
                print("Failed to upload", storageError)
                
            }
        }
    }
    
     func downloadImage (image: String, completion: @escaping (Result<UIImage, Error>)-> UIImage){
        Amplify.Storage.downloadData(key: image) { result in
            switch result {
            case .success(let imageData):
                let imageFin = UIImage(data: imageData)
                DispatchQueue.main.async {
                    self.imageCache[image] = imageFin
                    print("Trovata")
                }
                
            case .failure(_):
                print ("failed to download the image")
            }
            
            
        }
        
    }
}
