//
//  AuleStudioViewModel.swift
//  ForumApp
//
//  Created by Marco Longobardi on 10/09/21.
//

import Foundation
import SwiftUI

class AuleStudioViewModel : ObservableObject {
    var aulaScelta : Int = 1
    var data : Date = Date()
    
    private let auleEndpoint = "http://localhost:8080/api/auleStudio"

    
    func prenotazioneAule(user : String, token: String, completion: @escaping (Result<HTTPURLResponse, AuthError>) -> Void) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let result = formatter.string(from: data)
        
        let stringaRichiesta = "\(auleEndpoint)/\(aulaScelta)/prenotazione/\(user)"
        guard let urlRichiesta = URL(string: stringaRichiesta) else {
            completion(.failure(.custom(errorMessage: "URL is not correct")))
            return
        }
        
        let body : [String:String] = ["data": result]
        let finalBody = try! JSONSerialization.data(withJSONObject: body)
        
        var request = URLRequest(url: urlRichiesta)
        request.httpMethod = "POST"
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = finalBody
        
        URLSession.shared.dataTask(with: request){ (data, response, error) in
            
            guard let httpResponse = response as? HTTPURLResponse,
                  httpResponse.statusCode == 200
            else {
                completion(.failure(.custom(errorMessage: "No data")))
                return
            }
            completion(.success(httpResponse))
            
        }.resume()
        
    }
}
