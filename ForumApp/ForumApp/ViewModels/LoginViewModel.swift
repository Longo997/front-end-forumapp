//
//  LoginViewModel.swift
//  ForumApp
//
//  Created by Marco Longobardi on 08/09/21.
//

import Foundation
import SwiftUI

enum AuthError: Error {
    case invalidCredentials
    case custom(errorMessage: String)
}

struct LoginResponse: Codable{
    let token: String
    let user: UUID
    let ruolo: String
}

enum NetworkError: Error{
    case invalidURL
    case noData
    case decodingError
}

class LoginViewModel: ObservableObject {
    
    var mail: String = ""
    var password: String = ""
    private let loginEndpoint = "http://localhost:8080/api/utenti/login"

    @Published var isAuth: Bool = false
    
    func login() {
        
        let defaults = UserDefaults.standard
        
        performLogin(mail: mail, password: password) { result in
            switch result {
            case .success(let loginResponse):
                defaults.setValue(loginResponse.token, forKey: "token9")
                defaults.setValue(loginResponse.user.uuidString, forKey: "userID")
                defaults.setValue(loginResponse.ruolo, forKey: "ruolo")

                
                print(loginResponse.user.uuidString)
                DispatchQueue.main.async {
                    self.isAuth = true
                    print(self.isAuth)
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}

extension LoginViewModel {

    func performLogin(mail: String, password: String, completion: @escaping (Result<LoginResponse, AuthError>)-> Void) {
        guard let url = URL(string: loginEndpoint) else {
            completion(.failure(.custom(errorMessage: "URL is not correct")))
            return
        }
        guard let loginString = "\(mail):\(password)"
                .data(using: .utf8)?
                .base64EncodedString()
        else {
            fatalError("Failed to encode credentials")
        }
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("Basic \(loginString)", forHTTPHeaderField: "Authorization")
        
        URLSession.shared.dataTask(with: request){ (data, response,error) in
            guard let httpResponse = response as? HTTPURLResponse,
                  httpResponse.statusCode == 200,
                  let jsonData = data
            else {
                completion(.failure(.custom(errorMessage: "No data")))
                return
            }
            
            guard let loginResponse = try? JSONDecoder().decode(LoginResponse.self, from: jsonData) else {
                completion(.failure(.invalidCredentials))
                return
            }
            
            
            completion(.success(loginResponse))
            
        }.resume()
    }
}
