//
//  RegisterViewModel.swift
//  ForumApp
//
//  Created by Marco Longobardi on 08/09/21.
//

import Foundation

struct SignUpResponse: Codable{
    let token: String
    let user : UUID
    let ruolo : String
}


class RegisterViewModel: ObservableObject {
    
    var mail: String = ""
    var password: String = ""
    var nome: String = ""
    var cognome: String = ""
    var confirmPassword: String = ""
    private let signUpEndpoint = "http://localhost:8080/api/utenti"

    
    func register() {
        
        let defaults = UserDefaults.standard
        
        performSignUp(nome: nome, cognome: cognome, mail: mail, password: password) { result in
            switch result {
            case .success(let signUpResponse):
                defaults.setValue(signUpResponse.token, forKey: "token9")
                defaults.setValue(signUpResponse.user.uuidString, forKey: "userID")
                defaults.setValue(signUpResponse.ruolo, forKey: "ruolo")

                
               
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}

extension RegisterViewModel {
    
    func performSignUp(nome: String, cognome:String, mail:String, password: String,completion: @escaping (Result<SignUpResponse,AuthError>)-> Void){
        
        guard let baseUrl = URL(string: signUpEndpoint) else {
            print("Invalid URL...")
            return
        }
        
        let body : [String:String] = ["nome": nome, "cognome": cognome, "mail": mail, "password": password]
        let finalBody = try! JSONSerialization.data(withJSONObject: body)
        
        var request = URLRequest(url: baseUrl)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = finalBody
        
        URLSession.shared.dataTask(with: request){ data, response, error in
            
            guard let httpResponse = response as? HTTPURLResponse,
                  httpResponse.statusCode == 200,
                  let jsonData = data
            else {
                completion(.failure(.custom(errorMessage: "No data")))
                return
            }
            
            guard let signUpResponse = try? JSONDecoder().decode(SignUpResponse.self, from: jsonData) else {
                completion(.failure(.invalidCredentials))
                return
            }
            
            
            completion(.success(signUpResponse))
            print(signUpResponse)
        }.resume()
        
    }
}
