//
//  UserDetailsViewModel.swift
//  ForumApp
//
//  Created by Marco Longobardi on 08/09/21.
//

import Foundation

struct UserResponse: Codable{
    let id: UUID
    let nome: String
    let cognome: String
    let tipo: String
}

struct PrenotazioniAulaResponse: Codable {
    
    let data: String
    struct aula {
        let id: String
    }
}


class UserDetailsViewModel : ObservableObject {
    
    let urlBase = "http://localhost:8080/api/utenti/"
    
    @Published var utente = Utenti()
    
    @Published var eventiPrenotati : [Eventi] = [Eventi]()
    
    @Published var prenotazioniAula: [PrenotazioniAula] = [PrenotazioniAula]()

    func getInfo(token: String){
        
        
        guard let url = URL(string: urlBase + "me") else {
            print("Invalid url...")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            ///Assegnamo i valori ricevuti all'array news
            let user = try! JSONDecoder().decode(UserResponse.self, from: data!)
            
            ///DispatchQueue is an object that manages the execution of tasks serially or concurrently on your app’s main thread or on a background thread.
            DispatchQueue.main.async {
                self.utente = Utenti(id: user.id, nome: user.nome, cognome: user.cognome, ruolo: user.tipo)
            }
        }.resume()
    }
    
    func getEventiPrenotati (id: String, token: String) {
        
        guard let url = URL(string: urlBase + "\(id)" + "/eventi") else {
            print("Invalid url...")
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        
        print(id)
        print(token)
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            ///Assegnamo i valori ricevuti all'array news
            let eventi = try! JSONDecoder().decode([Eventi].self, from: data!)
            
            ///DispatchQueue is an object that manages the execution of tasks serially or concurrently on your app’s main thread or on a background thread.
            DispatchQueue.main.async {
                self.eventiPrenotati = eventi
            }
        }.resume()
    }
    
    func getPrenotazioniAula (id: String){
        guard let url = URL(string: "http://localhost:8080/api/auleStudio/1/\(id)/prenotazioni") else {
            print("Invalid url...")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        URLSession.shared.dataTask(with: request) { data, response, error in
            ///Assegnamo i valori ricevuti all'array news
            let prenotazioniAulaResp = try! JSONDecoder().decode([PrenotazioniAulaResponse].self, from: data!)
            var result : [PrenotazioniAula] = [PrenotazioniAula]()
            for prenotazione in prenotazioniAulaResp {
                result.append(PrenotazioniAula(id: UUID(), aula: "1", data: prenotazione.data))
                            }
            
            ///DispatchQueue is an object that manages the execution of tasks serially or concurrently on your app’s main thread or on a background thread.
            DispatchQueue.main.async {
                self.prenotazioniAula = result
                
            }
        }.resume()
        
    }
}
