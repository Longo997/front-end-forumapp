//
//  NewsViewModel.swift
//  ForumApp
//
//  Created by Marco Longobardi on 08/09/21.
//

import Foundation
import SwiftUI

class NewsViewModel : ObservableObject {
    
    @Published var news = [News]()

    func getNews() {
        guard let url = URL(string: "http://localhost:8080/api/news") else {
            print("Invalid url...")
            return
        }
        ///La classe URLSession permette di comunicare con l'endpoint dell'API indicato tramite l'URL. E' possibile produrne più di un'istanza
        ///in modo che ognuna coordini un gruppo di task correlate fra loro.URLSession has a singleton shared session for basic requests.
        ///DataTask crea una task che si occupa di prelevare i contenuti basati sulla richiesta per poi eseguire l'handler dopo il completamento.
        URLSession.shared.dataTask(with: url) { data, response, error in
            ///Assegnamo i valori ricevuti all'array news
            let news = try! JSONDecoder().decode([News].self, from: data!)
            
            ///DispatchQueue is an object that manages the execution of tasks serially or concurrently on your app’s main thread or on a background thread.
            DispatchQueue.main.async {
                
                self.news = news
            }
        }.resume()
        
    }

    
    func createNews(news: News,token: String, immagine: UIImage,completion: @escaping (Result<HTTPURLResponse, AuthError>) -> ()) {
        
        StorageService().uploadImage(immagine: immagine, nome: news.immagine)
        
        guard let baseUrl = URL(string: "http://localhost:8080/api/news") else {
            print("Invalid URL...")
            return
        }
        let body : [String:String] = ["titolo": news.titolo, "testo": news.testo, "dataPubblicazione": news.dataPubblicazione, "immagine": news.immagine + ".jpg"]
        let finalBody = try! JSONSerialization.data(withJSONObject: body)
        
        var request = URLRequest(url: baseUrl)
        request.httpMethod = "POST"
        request.addValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = finalBody
        
        URLSession.shared.dataTask(with: request){ data, response, error in
            
            guard let httpResponse = response as? HTTPURLResponse,
                  httpResponse.statusCode == 200
            else {
                completion(.failure(.custom(errorMessage: "\(String(describing: response))")))
                return
            }
            completion(.success(httpResponse))
            
        }.resume()
        
        
    }
    
}
