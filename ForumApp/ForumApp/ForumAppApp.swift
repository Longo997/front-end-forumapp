//
//  ForumAppApp.swift
//  ForumApp
//
//  Created by Marco Longobardi on 08/09/21.
//

import SwiftUI
import Amplify
import AmplifyPlugins

@main
struct ForumAppApp: App {
    
    init() {
        configureAmplify()
    }
    
    var body: some Scene {
        WindowGroup {
            HomeView()
        }
    }
    
    func configureAmplify(){
        do{
            //Storage
            try Amplify.add(plugin: AWSCognitoAuthPlugin())
            try Amplify.add(plugin: AWSS3StoragePlugin())
            
            try Amplify.configure()
            
            print("Amplify configurato con successo")
        } catch {
            print("Impossibile configurare Amplify", error)
        }
        
    }
}
