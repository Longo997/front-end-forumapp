//
//  Utente.swift
//  ForumApp
//
//  Created by Marco Longobardi on 08/09/21.
//

import Foundation

final class Utenti: Codable {
    var id: UUID
    var nome: String
    var cognome: String
    var ruolo:String
    
    init(id:UUID = UUID.init(), nome: String = "", cognome: String = "", ruolo: String = "") {
        self.id = id
        self.nome = nome
        self.cognome = cognome
        self.ruolo = ruolo
    }
}
