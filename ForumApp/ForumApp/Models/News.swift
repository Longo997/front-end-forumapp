//
//  News.swift
//  ForumApp
//
//  Created by Marco Longobardi on 08/09/21.
//

import Foundation
import SwiftUI

final class News: Codable, Identifiable {
    var id: UUID?
    var dataPubblicazione: String
    var titolo: String
    var testo: String
    var immagine: String
    
    
    
    init(id: UUID? ,data: String, titolo: String, testo: String = "", immagine: String) {
        self.id = id
        self.dataPubblicazione = data
        self.titolo = titolo
        self.testo = testo
        self.immagine = immagine
    }
    
    init(data: String, titolo: String, testo: String = "", immagine: String) {
        self.dataPubblicazione = data
        self.titolo = titolo
        self.testo = testo
        self.immagine = immagine
    }
}
