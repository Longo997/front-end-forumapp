//
//  Eventi.swift
//  ForumApp
//
//  Created by Marco Longobardi on 08/09/21.
//

import SwiftUI
import Foundation

final class Eventi: Codable, Identifiable {
    var id: UUID?
    var data: String
    var titolo: String
    var descrizione: String
    var numPosti: Int
    var immagine: String
    
    
    
    init(data: String, titolo: String, numPosti: Int, descrizione: String = "", immagine: String) {
        self.immagine = immagine
        self.data = data
        self.titolo = titolo
        self.descrizione = descrizione
        self.numPosti = numPosti
    }
    
    
}
