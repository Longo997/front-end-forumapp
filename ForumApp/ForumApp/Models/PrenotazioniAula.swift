//
//  PrenotazioniAula.swift
//  ForumApp
//
//  Created by Marco Longobardi on 10/09/21.
//

import Foundation
import SwiftUI

final class PrenotazioniAula: Codable, Identifiable {
    var id : UUID?
    var aula: String
    var data: String
    
    
    
    
    init(id: UUID?, aula: String ,data: String) {
        self.id = id
        self.aula = aula
        self.data = data
        
    }
    
}
